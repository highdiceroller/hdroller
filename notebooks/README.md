This collection of Jupyter notebooks gives several examples of using the `hdroller` module.

[Repository.](https://gitlab.com/highdiceroller/hdroller)

[API documentation.](https://highdiceroller.gitlab.io/hdroller/apidoc/hdroller.html)

If you have questions or comments, feel free to message me on [Reddit](https://www.reddit.com/user/HighDiceRoller) or [Twitter](https://twitter.com/highdiceroller).

## Tips (JupyterLite)

* Your local edits are stored in your browser. Only you can see your local version.
* You can restore the original version of a file by "deleting" it.
* If you want to export an edited or new notebook, you'll need to right click on the file and select "Download".
